import { useEffect, useState } from "react";

function UserForm() {
    const [firstName, setFirstName] = useState(localStorage.getItem("firstName"));
    const [lastName, setLastName] = useState(localStorage.getItem("lastName"));

    const onChangeFirstNameHandler = (event) => {
        console.log("firstname change ...");
        setFirstName(event.target.value);
    }

    const onChangeLastNameHandler = (event) => {
        console.log("lastname change ...");
        setLastName(event.target.value);
    }

    useEffect(() => {
        console.log("componentDidUpdated");
        localStorage.setItem("firstName", firstName);
        localStorage.setItem("lastName", lastName);
    })

    return (
        <div>
            <input value={firstName} onChange={onChangeFirstNameHandler} placeholder="firstname" /><br/>
            <input value={lastName} onChange={onChangeLastNameHandler} placeholder="lastname" />
            <p>{firstName} {lastName}</p>
        </div>
    )
}

export default UserForm;
